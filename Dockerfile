FROM ghcr.io/commandcracker/ffmpeg:latest AS ffmpeg

FROM ghcr.io/commandcracker/alpine-pypy3.10-pip:3.18.2-pypy-7.3.12-pip-23.1.2 AS builder

COPY requirements.txt .

RUN set -eux; \
	apk add --no-cache --update build-base; \
	pip install --no-cache-dir --upgrade pip; \
	pip install --no-cache-dir -r requirements.txt; \
	pip uninstall pip -y

FROM alpine:3.18.2

COPY --from=ffmpeg /usr/local /usr/local

WORKDIR /opt/bot

RUN set -eux; \
	apk add --no-cache --update \
	libffi \
	libexpat \
	libbz2 \
	ncurses \
	libgcc \
	libstdc++ \
	libgomp; \
	chown 1000 /opt/bot/

USER 1000

ADD src/ .

ENV \
	LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64 \
	XDG_CACHE_HOME="/opt/bot/.yt-dlp-cache" \
	PATH="/opt/pypy/bin:$PATH"

COPY --from=builder /opt/pypy /opt/pypy

CMD ["python", "bot.py"]
