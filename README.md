# <img src="music-logo-256x256.png" alt="drawing" width="32"/> Music Bot

A Discord Bot that plays music.

## Installation

Clone The Repository with [git](https://git-scm.com/downloads) `git clone https://gitlab.com/Commandcracker/music-bot.git` \
or download the Repository as a  [zip](https://gitlab.com/Commandcracker/music-bot/-/archive/master/music-bot-master.zip) and unzip it with [7-Zip](https://www.7-zip.org/) or `unzip music-bot-master.zip`

### Windows/Linux/Mac

install **[Python 3.7+](https://www.python.org/downloads/) and
[FFmpeg](https://ffmpeg.org/download.html) ([BtbN/FFmpeg-Builds](https://github.com/BtbN/FFmpeg-Builds/releases/tag/latest)) or [yt-dlp/FFmpeg-Builds](https://github.com/yt-dlp/FFmpeg-Builds/releases/tag/latest)**

#### install pip

```bash
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
```

#### install pip requirements

```bash
python -m pip install -r requirements.txt
```

### Docker

````bash
docker build -t music-bot .
docker run --restart=always music-bot -d
````

### Docker Compose

```bash
docker compose up
```

## Environment Variables

set them in your `.env` file!

### Required

`DISCORD_BOT_TOKEN`

The Token from your [discord bot](https://discord.com/developers/applications)

### Optional

`SPOTIPY_CLIENT_ID`

The Client ID from your [spotify application](https://developer.spotify.com/dashboard/applications)

`SPOTIPY_CLIENT_SECRET`

The Client Secret from your [spotify application](https://developer.spotify.com/dashboard/applications)

`PREFIX`

The Prefix for bot commands (the default prefix is `$`)

`LOGLEVEL`

The Log level for the logger (the default is `INFO`)

`NO_COLOR`

If it is set, the logger wound output colord messages
