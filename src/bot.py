#!/usr/bin/python3
# -*- coding: utf-8 -*-

# built-in modules
from sys import platform
from ctypes.util import find_library
from traceback import format_exception
from os import getenv
from logging import (
    Logger,
    StreamHandler,
    Formatter,
    getLogger,
    INFO
)
from typing import Union


# pip modules
from discord.ext.commands import (
    Bot,
    Context,
    Command,
    CommandError,
    NoPrivateMessage,
    UserInputError,
    Cog,
    CommandNotFound
)
from spotipy import (
    Spotify,
    SpotifyClientCredentials,
    MemoryCacheHandler
)
from discord import (
    Intents,
    Embed,
    Colour,
    HTTPException,
    Forbidden,
    opus,
    __version__ as discord_version
)
from discord.utils import setup_logging

# local modules
from messages import Messages
from colord_formatter import ColordFormatter
from colours import Foreground, reset


class MusicBot(Bot):
    def __init__(
        self,
        command_prefix: chr,
        intents: Intents,
        logger: Logger,
        **options
    ):
        super().__init__(command_prefix, help_command=None, intents=intents, **options)
        self.logger = logger

        @self.hybrid_command(
            name='aliases',
            description="Shows all aliases."
        )
        async def _aliases(ctx: Context):
            embed = Embed(
                color=Colour.blue(),
                title="Aliases"
            )

            for command in self.commands:
                if command:
                    command: Command

                    # Name
                    name = self.get_first_prefix() + command.name
                    embed.add_field(
                        name=name,
                        value=", ".join(
                            command.aliases
                        ) if command.aliases else "\u200B",
                        inline=False
                    )
            await ctx.send(embed=embed)

        @self.hybrid_command(
            name="help",
            aliases=['commands'],
            description="Get a list of all commands"
        )
        async def _help(ctx: Context):
            embed = Embed(
                color=Colour.blue(),
                title="Commands"
            )

            for command in self.commands:
                if command:
                    command: Command

                    # Name
                    name = self.get_first_prefix() + command.name
                    if command.usage:
                        name = name + " " + command.usage

                    # description
                    if command.description:
                        description = command.description
                    else:
                        description = "No Description"

                    embed.add_field(
                        name=name,
                        value=description,
                        inline=False
                    )

            await ctx.send(embed=embed)

    async def on_ready(self):
        self.logger.info('Logged in as %s', self.user)
        self.logger.debug('Bot id: %s', self.user.id)

        self.command_prefix = [
            self.command_prefix,
            "<@" + str(self.application_id) + ">"
        ]

    async def setup_hook(self):
        await self.load_extension("cogs.music")
        await self.tree.sync()
        self.logger.info("Successfully synced commands")

    def get_first_prefix(self):
        if isinstance(self.command_prefix, str):
            return self.command_prefix
        return self.command_prefix[0]

    async def handle_forbidden(self, ctx: Context):
        try:
            await ctx.send(**Messages.error("Im Missing Permissions."))
        except HTTPException:
            try:
                await ctx.author.send(**Messages.error("Im Missing Permissions."))
            except HTTPException:
                pass

    async def handle_no_private_message(self, ctx: Context):
        try:
            await ctx.author.send(**Messages.error(
                "This Command can not be used in Private Messages."
            ))
        except HTTPException:
            pass

    async def handle_user_input_error(self, ctx: Context):
        try:
            await ctx.send(**Messages.error(
                f"Usage: {self.get_first_prefix()}{ctx.command.name} "
                f"{ctx.command.usage}"
            ))
        except HTTPException:
            pass

    async def handle_unexpected_error(
        self,
        ctx: Context,
        error: CommandError
    ):
        self.logger.warning(f"Ignoring exception in command {ctx.command}:")
        trace = "".join(format_exception(
                type(error),
                error,
                error.__traceback__
        )).rstrip("\n")
        self.logger.warning(trace)

    async def handle_error(
            self,
            ctx: Context,
            error: CommandError
    ):
        if isinstance(error, NoPrivateMessage):
            await self.handle_no_private_message(ctx)
        elif isinstance(error, Forbidden):
            await self.handle_forbidden(ctx)
        elif isinstance(error, UserInputError):
            await self.handle_user_input_error(ctx)
        else:
            await self.handle_unexpected_error(ctx, error)

    async def on_command_error(
        self,
        ctx: Context,
        error: CommandError
    ):
        """The event triggered when an error is raised while invoking a command.
        Parameters
        ------------
        ctx: commands.Context
            The context used for command invocation.
        error: commands.CommandError
            The Exception raised.
        """

        if self.extra_events.get('on_command_error', None):
            return

        # This prevents any commands with local handlers
        # being handled here in on_command_error.
        if hasattr(ctx.command, 'on_error'):
            return

        # This prevents any cogs with an overwritten cog_command_error
        # being handled here.
        cog: Cog = ctx.cog
        # noinspection PyProtectedMember
        if cog and cog._get_overridden_method(cog.cog_command_error) is not None:
            return

        # get the original exception
        error = getattr(error, 'original', error)

        # Anything in ignored will return and prevent anything happening.
        ignored = (CommandNotFound,)
        if isinstance(error, ignored):
            return

        await self.handle_error(ctx, error)


def _load_default_opus() -> bool:
    try:
        opus._load_default()
        return True
    except Exception:
        return False


def _load_opus(library_path: str) -> bool:
    try:
        opus.load_opus(library_path)
        return True
    except Exception:
        return False


def load_opus() -> bool:
    if opus.is_loaded():
        return True

    if platform == "win32":
        return _load_default_opus()

    for library_name in {"opus", "libopus"}:
        if final_library_name := find_library(library_name) is not None:
            return _load_opus(final_library_name)

    for library_path in {"libopus.so", "opus.so"}:
        if _load_opus(library_path):
            return True

    return False


def prepend_to_substring(string: str, substring: str, prepend: str) -> str:
    return string.replace(substring,  prepend + substring)


def prepend_to_substrings(
    string: str,
    substrings: Union[list, dict, set],
    prepend: str
) -> str:
    for substring in substrings:
        string = prepend_to_substring(string, substring, prepend)
    return string


def print_banner():
    banner = (
        "\n" +
        " ███╗   ███╗██╗   ██╗███████╗██╗ ██████╗    ██████╗  ██████╗ ████████╗\n" +
        " ████╗ ████║██║   ██║██╔════╝██║██╔════╝    ██╔══██╗██╔═══██╗╚══██╔══╝\n" +
        " ██╔████╔██║██║   ██║███████╗██║██║         ██████╔╝██║   ██║   ██║\n" +
        " ██║╚██╔╝██║██║   ██║╚════██║██║██║         ██╔══██╗██║   ██║   ██║\n" +
        " ██║ ╚═╝ ██║╚██████╔╝███████║██║╚██████╗    ██████╔╝╚██████╔╝   ██║\n" +
        " ╚═╝     ╚═╝ ╚═════╝ ╚══════╝╚═╝ ╚═════╝    ╚═════╝  ╚═════╝    ╚═╝\n"
    )

    if not getenv("NO_COLOR"):
        banner = prepend_to_substrings(
            prepend_to_substring(banner, '█', Foreground.BLUE),
            ['╔', '╚', '═', '║', '╗', '╝'],
            Foreground.BRIGHT_BLACK
        )

    print(banner, flush=True)


def main():
    print_banner()

    # Logger Setup
    logger = getLogger(__name__)
    log_level = getenv("LOGLEVEL") or INFO
    logger.setLevel(log_level)
    logging_handler = StreamHandler()
    # noinspection SpellCheckingInspection
    if getenv("NO_COLOR"):
        formatter = Formatter(
            fmt="[%(asctime)s %(levelname)s] [MusicBot] %(message)s",
            datefmt="%H:%M:%S"
        )
    else:
        formatter = ColordFormatter(
            fmt="[%(asctime)s %(levelname)s] "
                f"{Foreground.BLUE}[MusicBot]"
                f"{reset()} %(message)s",
            datefmt="%H:%M:%S"
        )

    logging_handler.setFormatter(formatter)
    logger.addHandler(logging_handler)

    # discord logger
    setup_logging(level=log_level, root=False)
    discord_logger = getLogger('discord')

    handler = discord_logger.handlers[1]
    if getenv("NO_COLOR"):
        handler.formatter = Formatter(
            fmt="[%(asctime)s %(levelname)s] [discord.%(module)s] %(message)s",
            datefmt="%H:%M:%S"
        )
    else:
        handler.formatter = ColordFormatter(
            fmt="[%(asctime)s %(levelname)s] "
                f"{Foreground.MAGENTA}[discord.%(module)s]"
                f"{reset()} %(message)s",
            datefmt="%H:%M:%S"
        )

    # load opus
    if not load_opus():
        logger.fatal("Failed to load libopus")
        exit()

    # Spotify
    spotify_client_id = getenv("SPOTIPY_CLIENT_ID")
    spotify_client_secret = getenv("SPOTIPY_CLIENT_SECRET")
    spotipy = None

    if spotify_client_id and spotify_client_secret:
        logger.info("Spotipy Enabled")
        spotipy = Spotify(
            auth_manager=SpotifyClientCredentials(
                client_id=spotify_client_id,
                client_secret=spotify_client_secret,
                cache_handler=MemoryCacheHandler()
            )
        )
    else:
        logger.info("Spotipy Disabled")

    # Bot
    prefix = getenv("PREFIX") or '$'

    intents = Intents.default()
    intents.message_content = True

    bot = MusicBot(
        case_insensitive=True,
        strip_after_prefix=True,
        command_prefix=prefix,
        intents=intents,
        logger=logger
    )
    bot.spotipy = spotipy

    logger.debug('Discord version: %s', discord_version)

    token = getenv("DISCORD_BOT_TOKEN")
    if token is None:
        logger.critical("DISCORD_BOT_TOKEN is required")
    else:
        logger.info('Logging in ...')
        bot.run(token, log_handler=None)


if __name__ == '__main__':
    main()
