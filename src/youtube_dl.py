#!/usr/bin/python3
# -*- coding: utf-8 -*-

# built-in modules
from re import search as re_search
from functools import partial
from asyncio import get_event_loop
from os import getenv
from logging import (
    Formatter,
    StreamHandler,
    getLogger,
    Logger,
    INFO
)
from typing import MutableMapping
from urllib.request import Request, urlopen
from urllib.error import URLError
from json import loads, JSONDecodeError

# pip modules
from yt_dlp.utils import DownloadError
from yt_dlp import YoutubeDL
from discord.ext import commands
from spotipy import Spotify
from discord import (
    PCMVolumeTransformer,
    AudioSource,
    FFmpegPCMAudio
)

# local modules
from messages import Messages
from data import GuildData, Queue, YoutubeSong, SpotifySong
from colord_formatter import ColordFormatter
from colours import Foreground, reset


def get_formatter():
    if getenv("NO_COLOR"):
        return Formatter(
            fmt="[%(asctime)s %(levelname)s] [YTDL] %(message)s",
            datefmt="%H:%M:%S"
        )

    return ColordFormatter(
        fmt="[%(asctime)s %(levelname)s] "
        f"{Foreground.RED}[YTDL]"
            f"{reset()} %(message)s",
            datefmt="%H:%M:%S"
    )


def setup_logger() -> Logger:
    logger = getLogger(__name__)
    logger.setLevel(getenv("LOGLEVEL", INFO))

    logging_handler = StreamHandler()
    logging_handler.setFormatter(get_formatter())
    logger.addHandler(logging_handler)

    return logger


def beautify_msg(msg: str) -> str:
    if msg.startswith("\033[0;31mERROR:\033[0m "):
        msg = msg[18:]
    if msg.startswith("[youtube] "):
        return msg[10:]
    return msg


class ResponseCodeNotOK(URLError):
    def __init__(self, reason, code):
        URLError.__init__(self, reason)
        self.code = code


def make_get_request(
    url: str,
    headers: MutableMapping[str, str] = {'User-Agent': 'urllib'},
    timeout: int = 2
) -> str:
    with urlopen(Request(url, headers=headers), timeout=timeout) as response:
        if response.getcode() == 200:
            return response.read().decode('utf-8')
        raise ResponseCodeNotOK(response.reason, response.getcode())


def get_dislikes(videoId: str) -> int:
    try:
        return loads(make_get_request(
            "https://returnyoutubedislikeapi.com/Votes?videoId=" + videoId
        )).get("dislikes")
    except (URLError, TypeError, JSONDecodeError):
        pass


class YTLogger:

    def __init__(self) -> None:
        logger = getLogger(__name__)
        if logger.hasHandlers():
            self.logger = logger
        else:
            self.logger = setup_logger()

    def debug(self, msg):
        # For compatibility with youtube-dl, both debug and info are passed into debug
        # You can distinguish them by the prefix '[debug] '
        if msg.startswith('[debug] '):
            self.logger.debug(beautify_msg(msg))
        else:
            self.info(msg)

    def info(self, msg):
        self.logger.info(beautify_msg(msg))

    def warning(self, msg):
        self.logger.warning(beautify_msg(msg))

    def error(self, msg):
        self.logger.error(beautify_msg(msg))


class YTDLSource(PCMVolumeTransformer):
    # noinspection SpellCheckingInspection
    YDL_OPTIONS = {
        'logger': YTLogger(),
        'format': 'bestaudio/best',
        'outtmpl': '{}',
        'restrictfilenames': True,
        'noplaylist': False,
        'nocheckcertificate': True,
        'extract_flat': 'in_playlist',
        'default_search': 'auto',
        'source_address': '0.0.0.0'  # ipv6 addresses cause issues sometimes
    }

    FFMPEG_BEFORE_OPTIONS = '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5'
    FFMPEG_OPTIONS = "-vn"

    ytdl = YoutubeDL(YDL_OPTIONS)

    def __init__(
        self,
        source: AudioSource,
        *,
        data: dict,
        requester,
        volume: float = 0.05
    ):
        super().__init__(source, volume)
        self.requester = requester

        self.title = data.get('title')
        self.web_url = data.get('webpage_url')
        self.data = data

    @classmethod
    async def from_youtube(
            cls,
            ctx: commands.Context,
            search: str,
            logger: Logger,
            guild_data: GuildData,
            *,
            loop=None
    ):
        loop = loop or get_event_loop()

        try:
            data: dict = await loop.run_in_executor(None, partial(
                cls.ytdl.extract_info,
                url=search,
                download=False
            ))
        except DownloadError as ex:
            return ex

        # debug
        log_data = data.copy()
        try:
            log_data.pop("formats")
            log_data.pop("thumbnails")
            log_data.pop("automatic_captions")
        except KeyError:
            pass

        logger.debug(data)

        guild_data.add_if_empty(ctx.guild.id)

        # if playlist
        if 'entries' in data:
            if data.get("extractor_key") != "YoutubeSearch":
                # Queue Playlist
                queue: Queue = guild_data.get(ctx.guild.id)["queue"]

                for entry in data['entries'][1:]:
                    queue.append(YoutubeSong(id=entry.get(
                        'id'), title=entry.get('title')))

                # Playlist info
                await ctx.send(**Messages.playlist(data=data))

            if len(data['entries']) <= 0:
                return "No video found"

            # Take first item from a playlist
            return await cls.from_auto(
                ctx=ctx,
                logger=logger,
                guild_data=guild_data,
                search=data['entries'][0]['url'],
                loop=loop
            )

        bassboost = guild_data.get(ctx.guild.id).get("bassboost") or False
        volume = guild_data.get(ctx.guild.id).get("volume") or 0.05

        options = cls.FFMPEG_OPTIONS

        if bassboost:
            options = f"{options} -filter bass=gain=10"

        if not data.get("dislike_count"):
            data["dislike_count"] = get_dislikes(data.get('id'))

        return cls(
            source=FFmpegPCMAudio(
                source=data['url'],
                before_options=cls.FFMPEG_BEFORE_OPTIONS,
                options=options
            ),
            data=data,
            requester=ctx.author,
            volume=volume
        )

    @classmethod
    async def from_spotify_playlist(
            cls,
            url: str,
            spotipy: Spotify,
            guild_data: GuildData,
            logger: Logger,
            ctx: commands.Context,
            *,
            loop=None
    ):
        loop = loop or get_event_loop()

        playlist = spotipy.playlist(url)
        playlist_items = spotipy.playlist_items(url)

        await ctx.send(**Messages.spotify_playlist(
            data=playlist,
            tracks=len(playlist_items["items"])
        ))

        guild_data.add_if_empty(ctx.guild.id)
        queue: Queue = guild_data.get(ctx.guild.id)["queue"]

        for item in playlist_items["items"][1:]:
            queue.append(SpotifySong(
                id=item["track"]["id"],
                title=item["track"]["name"],
                artist=item["track"]["artists"][0]["name"]
            ))

        return await cls.from_youtube(
            search=SpotifySong.make_search_term(
                playlist_items["items"][0]["track"]["artists"][0]["name"],
                playlist_items["items"][0]["track"]["name"]
            ),
            loop=loop,
            guild_data=guild_data,
            logger=logger,
            ctx=ctx
        )

    @classmethod
    async def from_data(
            cls,
            data: dict,
            guild_data: GuildData,
            ctx: commands.Context
    ):
        bassboost = guild_data.get(ctx.guild.id).get("bassboost") or False
        volume = guild_data.get(ctx.guild.id).get("volume") or 0.05

        options = cls.FFMPEG_OPTIONS

        if bassboost:
            options = f"{options} -filter bass=g=20,dynaudnorm=f=200"

        return cls(
            source=FFmpegPCMAudio(
                source=data['url'],
                before_options=cls.FFMPEG_BEFORE_OPTIONS,
                options=options
            ),
            data=data,
            requester=ctx.author,
            volume=volume
        )

    @classmethod
    async def from_spotify_track(
            cls,
            url: str,
            spotipy: Spotify,
            guild_data: GuildData,
            logger: Logger,
            ctx: commands.Context,
            *,
            loop=None
    ):
        loop = loop or get_event_loop()
        track: dict = spotipy.track(url)
        return await cls.from_youtube(
            search=SpotifySong.make_search_term(
                track["artists"][0]["name"],
                track["name"]
            ),
            loop=loop,
            guild_data=guild_data,
            logger=logger,
            ctx=ctx
        )

    @classmethod
    async def from_spotify_album(
            cls,
            url: str,
            spotipy: Spotify,
            guild_data: GuildData,
            logger: Logger,
            ctx: commands.Context,
            *,
            loop=None
    ):
        loop = loop or get_event_loop()

        album_tracks = spotipy.album_tracks(url)
        album = spotipy.album(url)

        await ctx.send(**Messages.spotify_album(album, len(album_tracks["items"])))

        guild_data.add_if_empty(ctx.guild.id)
        queue: Queue = guild_data.get(ctx.guild.id)["queue"]

        for item in album_tracks["items"][1:]:
            queue.append(SpotifySong(
                id=item["id"], title=item["name"], artist=item["artists"][0]["name"]))

        return await cls.from_youtube(
            search=SpotifySong.make_search_term(
                album_tracks["items"][0]["artists"][0]["name"],
                album_tracks["items"][0]["name"]
            ),
            loop=loop,
            guild_data=guild_data,
            logger=logger,
            ctx=ctx
        )

    @classmethod
    async def from_spotify_artist(
            cls,
            url: str,
            spotipy: Spotify,
            guild_data: GuildData,
            logger: Logger,
            ctx: commands.Context,
            *,
            loop=None
    ):
        loop = loop or get_event_loop()

        artist = spotipy.artist(url)
        artist_top_tracks: dict = spotipy.artist_top_tracks(url)

        await ctx.send(**Messages.spotify_artist(
            artist,
            len(artist_top_tracks["tracks"])
        ))

        guild_data.add_if_empty(ctx.guild.id)
        queue: Queue = guild_data.get(ctx.guild.id)["queue"]

        for item in artist_top_tracks["tracks"][1:]:
            queue.append(SpotifySong(
                id=item["id"], title=item["name"], artist=item["artists"][0]["name"]))

        return await cls.from_youtube(
            search=SpotifySong.make_search_term(
                artist_top_tracks["tracks"][0]["artists"][0]["name"],
                artist_top_tracks["tracks"][0]["name"]
            ),
            loop=loop,
            guild_data=guild_data,
            logger=logger,
            ctx=ctx
        )

    @classmethod
    async def from_auto(
            cls,
            ctx: commands.Context,
            search: str,
            logger: Logger,
            guild_data: GuildData,
            *,
            loop=None,
            spotipy: Spotify = None
    ):
        loop = loop or get_event_loop()

        if re_search(r"^https://", search):
            if re_search(r"^https://[a-z]+\.spotify\.com/", search):
                if spotipy is None:
                    return "Spotify is Disabled"
                elif re_search(r"^https://[a-z]+\.spotify\.com/playlist", search):
                    return await cls.from_spotify_playlist(
                        ctx=ctx,
                        url=search,
                        logger=logger,
                        guild_data=guild_data,
                        loop=loop,
                        spotipy=spotipy
                    )
                elif re_search(r"^https://[a-z]+\.spotify\.com/track", search):
                    return await cls.from_spotify_track(
                        ctx=ctx,
                        url=search,
                        logger=logger,
                        guild_data=guild_data,
                        loop=loop,
                        spotipy=spotipy
                    )
                elif re_search(r"^https://[a-z]+\.spotify\.com/artist", search):
                    return await cls.from_spotify_artist(
                        ctx=ctx,
                        url=search,
                        logger=logger,
                        guild_data=guild_data,
                        loop=loop,
                        spotipy=spotipy
                    )
                elif re_search(r"^https://[a-z]+\.spotify\.com/album", search):
                    return await cls.from_spotify_album(
                        ctx=ctx,
                        url=search,
                        logger=logger,
                        guild_data=guild_data,
                        loop=loop,
                        spotipy=spotipy
                    )
                else:
                    return "Unsupported spotify url."
            else:
                return await cls.from_youtube(
                    ctx=ctx,
                    search=search,
                    logger=logger,
                    guild_data=guild_data,
                    loop=loop
                )
        elif re_search(r"^spotify:\w+:\w+", search):
            if spotipy is None:
                return "Spotify is Disabled"
            elif re_search(r"^spotify:playlist:\w+", search):
                return await cls.from_spotify_playlist(
                    ctx=ctx,
                    url=search,
                    logger=logger,
                    guild_data=guild_data,
                    loop=loop,
                    spotipy=spotipy
                )
            elif re_search(r"^spotify:track:\w+", search):
                return await cls.from_spotify_track(
                    ctx=ctx,
                    url=search,
                    logger=logger,
                    guild_data=guild_data,
                    loop=loop,
                    spotipy=spotipy
                )
            elif re_search(r"^spotify:artist:\w+", search):
                return await cls.from_spotify_artist(
                    ctx=ctx,
                    url=search,
                    logger=logger,
                    guild_data=guild_data,
                    loop=loop,
                    spotipy=spotipy
                )
            elif re_search(r"^spotify:album:\w+", search):
                return await cls.from_spotify_album(
                    ctx=ctx,
                    url=search,
                    logger=logger,
                    guild_data=guild_data,
                    loop=loop,
                    spotipy=spotipy
                )
            else:
                return "Unsupported spotify uri."
        else:
            return await cls.from_youtube(
                ctx=ctx,
                search=search,
                logger=logger,
                guild_data=guild_data,
                loop=loop
            )
