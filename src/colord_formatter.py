#!/usr/bin/python3
# -*- coding: utf-8 -*-

# built-in modules
from logging import (
    Formatter,
    DEBUG,
    INFO,
    WARNING,
    ERROR,
    CRITICAL
)

# local modules
from colours import Foreground, reset


class ColordFormatter(Formatter):
    """Logging colored formatter, adapted from https://stackoverflow.com/a/56944256/3638629"""

    # noinspection SpellCheckingInspection
    def __init__(self, fmt=None, datefmt=None):
        super().__init__()
        self.fmt = fmt
        self.datefmt = datefmt
        self.FORMATS = {
            DEBUG: Foreground.BRIGHT_BLACK + self.fmt + reset(),
            INFO: Foreground.BRIGHT_WHITE + self.fmt + reset(),
            WARNING: Foreground.BRIGHT_YELLOW + self.fmt + reset(),
            ERROR: Foreground.BRIGHT_RED + self.fmt + reset(),
            CRITICAL: Foreground.RED + self.fmt + reset()
        }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = Formatter(log_fmt, datefmt=self.datefmt)
        return formatter.format(record)
