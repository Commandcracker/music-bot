#!/usr/bin/python3
# -*- coding: utf-8 -*-

# built-in modules
from dataclasses import dataclass
from random import shuffle


@dataclass
class Song(object):
    title: str

    def get_url(self) -> str:
        return self.title

    def get_search_term(self) -> str:
        return self.title


@dataclass
class YoutubeSong(Song):
    id: str

    def get_url(self) -> str:
        return f"https://www.youtube.com/watch?v={self.id}"

    def get_search_term(self) -> str:
        return self.id


@dataclass
class SpotifySong(Song):
    id: str
    artist: str

    def get_url(self) -> str:
        return f"https://open.spotify.com/track/{self.id}"

    @staticmethod
    def make_search_term(artist, title) -> str:
        return f"{artist} - {title}"

    def get_search_term(self) -> str:
        return self.make_search_term(self.artist, self.title)


class Queue(list):
    def is_empty(self) -> bool:
        return not len(self) > 0

    def shuffle(self):
        shuffle(self)


# TODO: Implement GuildDataContainer
"""
@dataclass
class GuildData(object):
    loop: object = None
    bassboost: bool = False
    volume: float = 0.05
    queue: Queue = None

    def __post_init__(self):
        self.queue = Queue()


class GuildDataContainer(dict):
    def remove(self, guild_id: int):
        del self[guild_id]

    def add(self, guild_id: int):
        self[guild_id] = GuildData()

    def is_empty(self, guild_id: int) -> bool:
        if self.get(guild_id) is None:
            return True
        else:
            return not len(self[guild_id]) > 0

    def add_if_empty(self, guild_id: int):
        if self.is_empty(guild_id):
            self.add(guild_id)
            
@commands.guild_only()
@commands.command(name='bassboost', description="boot's the bass for the next music.")
async def _bassboost(self, ctx: commands.Context):
    self.guild_data_container.add_if_empty(ctx.guild.id)
    guild_data: dict = self.guild_data_container.get(ctx.guild.id)
    bassboost = guild_data.get("bassboost") or False
    guild_data["bassboost"] = not bassboost

    if not bassboost:
        await ctx.send(**Messages.successfully("Enabled bassboost"))
    else:
        await ctx.send(**Messages.successfully("Disabled bassboost"))
"""


class GuildData(dict):
    def remove_guild(self, guild_id: int):
        del self[guild_id]

    def add_guild(self, guild_id: int, data: dict = None):
        if data is None:
            data = {"queue": Queue()}
        self[guild_id] = data

    def is_empty(self, guild_id: int) -> bool:
        if self.get(guild_id) is None:
            return True
        return not len(self[guild_id]) > 0

    def add_if_empty(self, guild_id: int):
        if self.is_empty(guild_id):
            self.add_guild(guild_id)
