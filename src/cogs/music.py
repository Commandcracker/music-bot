#!/usr/bin/python3
# -*- coding: utf-8 -*-

# built-in modules
from random import choice

# pip modules
from discord import (
    Client,
    TextChannel,
    VoiceClient,
    Colour,
    Embed,
    Activity,
    ActivityType,
    Member,
    VoiceState,
    ButtonStyle,
    Interaction
)
from discord.app_commands import describe
from discord.ui import View, Button
from discord.utils import get
from discord.ext.commands import (
    Cog,
    Context,
    guild_only,
    hybrid_command
)
from discord.ext.tasks import loop
from spotipy import Spotify


# local modules
from messages import Messages
from youtube_dl import YTDLSource, beautify_msg
from data import Song, Queue, GuildData

char_to_keycap = {
    0: "\u0030\uFE0F\u20E3",  # 0️⃣ :zero:
    1: "\u0031\uFE0F\u20E3",  # 1️⃣ :one:
    2: "\u0032\uFE0F\u20E3",  # 2️⃣ :two:
    3: "\u0033\uFE0F\u20E3",  # 3️⃣ :three:
    4: "\u0034\uFE0F\u20E3",  # 4️⃣ :four:
    5: "\u0035\uFE0F\u20E3",  # 5️⃣ :five:
    6: "\u0036\uFE0F\u20E3",  # 6️⃣ :six:
    7: "\u0037\uFE0F\u20E3",  # 7️⃣ :seven:
    8: "\u0038\uFE0F\u20E3",  # 8️⃣ :eight:
    9: "\u0039\uFE0F\u20E3",  # 9️⃣ :nine:
    10: "\U0001F51F",  # 🔟 :keycap_ten:
    # "*": "\u002A\uFE0F\u20E3",  # *️⃣ :asterisk:
    # "#": "\u0023\uFE0F\u20E3"  # #️⃣ :hash:
}


class InteractionToContext(object):
    def __init__(self, interaction: Interaction):
        self.guild = interaction.guild
        self.send = interaction.channel.send


class SkipButtonCallbackBuilder(object):
    def __init__(self, skip_to_indedx, _skip, view):
        self.skip_to_indedx = skip_to_indedx
        self._skip = _skip
        self.view = view

    async def callback(self, interaction: Interaction):
        await self._skip(
            InteractionToContext(interaction),
            self.skip_to_indedx
        )
        self.view.stop()


class MusicCog(Cog):
    activities = [
        "Nightcore",
        "Heavy Metal",
        "Metal",
        "Punk",
        "Jazz",
        "Snake Jazz",  # joke
        "Rap",
        "Hip Hop",
        "Rock & roll",
        "Techno",
        "Technoblade",  # joke
        "R&B",
        "Electro"
    ]

    def __init__(self, bot: Client, spotipy: Spotify = None):
        self.bot = bot
        self.logger = self.bot.logger
        self.guild_data = GuildData()
        self.spotipy = spotipy

    # TODO: error ??
    async def player_callback(
        self,
        error,
        guild_id: int,
        channel: TextChannel,
        ctx: Context
    ):
        if self.guild_data.is_empty(guild_id):
            return

        if (
            self.guild_data.get(guild_id).get("loop") and
            self.guild_data.get(guild_id).get("loop") != "skip"
        ):
            voice_client: VoiceClient = get(
                self.bot.voice_clients, server_id=guild_id)

            if voice_client is None:
                return

            voice_client.play(
                source=await YTDLSource.from_data(
                    data=self.guild_data.get(guild_id).get("loop"),
                    guild_data=self.guild_data,
                    ctx=ctx
                ),
                after=lambda _error: self.bot.loop.create_task(
                    self.player_callback(
                        error=_error,
                        guild_id=guild_id,
                        channel=channel,
                        ctx=ctx
                    )
                )
            )
            return

        queue: Queue = self.guild_data.get(guild_id)["queue"]

        if queue.is_empty():
            self.guild_data.remove_guild(guild_id)
            voice_client: VoiceClient = get(
                self.bot.voice_clients,
                guild=self.bot.get_guild(guild_id)
            )
            if voice_client and voice_client.is_connected():
                await voice_client.disconnect()
            return

        voice_client: VoiceClient = get(
            self.bot.voice_clients, server_id=guild_id)

        if voice_client is None:
            return

        song: Song = queue[0]

        player = await YTDLSource.from_auto(
            ctx=ctx,
            search=song.get_search_term(),
            loop=self.bot.loop,
            guild_data=self.guild_data,
            logger=self.logger,
            spotipy=self.spotipy
        )

        if self.guild_data.get(guild_id).get("loop") == "skip":
            self.guild_data.get(guild_id)["loop"] = player.data

        del queue[0]

        if isinstance(player, YTDLSource):
            await ctx.send(**Messages.playing(player.data))
            voice_client.play(
                source=player,
                after=lambda _error: self.bot.loop.create_task(
                    self.player_callback(
                        error=_error,
                        guild_id=guild_id,
                        channel=channel,
                        ctx=ctx
                    )
                )
            )
        else:
            embed = Embed(
                color=Colour.red(),
                title=song.title,
                url=song.get_url(),
                description=beautify_msg(str(player))
            )
            embed.set_author(name="Error")

            await ctx.send(embed=embed)
            await self.player_callback(
                error=None,
                guild_id=guild_id,
                channel=channel,
                ctx=ctx
            )

    @loop(minutes=1)
    async def _activity(self):
        await self.bot.change_presence(
            activity=Activity(
                type=ActivityType.listening,
                name=choice(self.activities)
            )
        )

    @Cog.listener()
    async def on_ready(self):
        self.logger.info('Loaded music Cog.')
        self._activity.start()
        self.logger.info('Started activity tasks.')
        self.logger.info('Ready!')

    @Cog.listener()
    async def on_voice_state_update(
        self,
        member: Member,
        before: VoiceState,
        after: VoiceState
    ):
        # Cleanup if bot has been kicked from channel
        if member.id == self.bot.user.id:
            voice_client: VoiceClient = get(
                self.bot.voice_clients,
                guild=member.guild
            )

            if (
                self.guild_data.get(member.guild.id) and
                not self.guild_data.get(member.guild.id).get("loop") == "skip"
            ):
                if (
                        voice_client and (
                            voice_client.is_connected() is False and
                            voice_client.is_playing()
                        )):
                    voice_client.cleanup()
                    if not self.guild_data.is_empty(member.guild.id):
                        self.guild_data.remove_guild(member.guild.id)

        # leave if noone is in the channel
        if before.channel:
            if self.bot.user in before.channel.members:
                count = 0
                for member in before.channel.members:
                    if not member.bot:
                        count += 1
                if count <= 0:
                    voice_client: VoiceClient = get(
                        self.bot.voice_clients,
                        guild=member.guild
                    )
                    await voice_client.disconnect()
                    if not self.guild_data.is_empty(member.guild.id):
                        self.guild_data.remove_guild(member.guild.id)

    @guild_only()
    @hybrid_command(
        name='join',
        aliases=['summon', 'connect'],
        description="Make the bot join your channel"
    )
    async def _join(self, ctx: Context):
        if not ctx.author.voice:
            await ctx.send(**Messages.error(
                "You have to be in a voice channel to use this Command."
            ))
        else:
            channel = ctx.author.voice.channel
            voice_client: VoiceClient = get(
                self.bot.voice_clients,
                guild=ctx.guild
            )
            if (
                voice_client and
                voice_client.is_connected() and
                voice_client.channel == channel
            ):
                await ctx.send(**Messages.error("I'm already in your voice channel."))
            else:
                if voice_client and voice_client.is_connected():
                    await voice_client.move_to(channel)
                else:
                    await channel.connect()
                await ctx.send(**Messages.successfully("Joined your channel."))

    @guild_only()
    @hybrid_command(
        name='disconnect',
        aliases=['dc', 'leave', 'dis'],
        description="Make the bot disconnect from your channel"
    )
    async def _disconnect(self, ctx: Context):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )
        if voice_client and voice_client.is_connected():
            await voice_client.disconnect()
            await ctx.send(**Messages.successfully("Disconnected."))
        else:
            await ctx.send(**Messages.error("I am not connected to a voice channel."))

        if not self.guild_data.is_empty(ctx.guild.id):
            self.guild_data.remove_guild(ctx.guild.id)

    @guild_only()
    @hybrid_command(
        name='volume',
        aliases=['vol'],
        description="set the music volume",
        usage="[volume]"
    )
    @describe(volume='The volume')
    async def _volume(self, ctx: Context, volume: int):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )
        if voice_client is None:
            await ctx.send(**Messages.error("Im not connected to a voice channel."))
        else:
            await ctx.send(**Messages.successfully(
                "Changed volume from " +
                str(int(voice_client.source.volume * 100)) +
                "% to " + str(volume) + "%."
            ))
            self.guild_data.add_if_empty(ctx.guild.id)
            self.guild_data.get(ctx.guild.id)["volume"] = volume / 100
            voice_client.source.volume = volume / 100

    @guild_only()
    @hybrid_command(
        name='nowplaying',
        aliases=['np'],
        description="Shows what song the bot is currently playing."
    )
    async def _nowplaying(self, ctx: Context):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )
        if not (voice_client and voice_client.is_playing()):
            await ctx.send(**Messages.error("The bot isn't playing anything."))
        else:
            await ctx.send(**Messages.playing(voice_client.source.data))

    @guild_only()
    @hybrid_command(
        name='pause',
        aliases=['stop'],
        description="Pause the music"
    )
    async def _pause(self, ctx: Context):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )
        if not (voice_client and voice_client.is_playing()):
            await ctx.send(**Messages.error("There is nothing to pause."))
        else:
            voice_client.pause()
            await ctx.send(**Messages.successfully("Paused."))

    @guild_only()
    @hybrid_command(
        name='resume',
        aliases=['re', 'res', 'continue', 'start'],
        description="Resume the music"
    )
    async def _resume(self, ctx: Context):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )
        if not (voice_client and voice_client.is_paused()):
            await ctx.send(**Messages.error("There is nothing to resume."))
        else:
            voice_client.resume()
            await ctx.send(**Messages.successfully("Resumed."))

    @guild_only()
    @hybrid_command(
        name='description',
        aliases=['desc', 'descr', "des", "descp"],
        description="Displays the description of the current song."
    )
    async def _description(self, ctx: Context):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )
        if not (voice_client and voice_client.is_playing()):
            await ctx.send(**Messages.error("The bot isn't playing anything."))
        else:
            data = voice_client.source.data
            embed = Messages.playing(data)["embed"]
            embed.set_author(name="Description")
            embed.description = data.get("description")
            await ctx.send(embed=embed)

    @guild_only()
    @hybrid_command(
        name='play',
        aliases=['p'],
        description="Play a song",
        usage="[url]"
    )
    @describe(
        url="The url"
    )
    async def _play(self, ctx: Context, *, url: str):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )

        if not (voice_client and voice_client.is_connected()):
            if not ctx.author.voice:
                await ctx.send(**Messages.error(
                    "You have to be in a voice channel to use this Command."
                ))
                return
            await ctx.author.voice.channel.connect()

        elif voice_client.is_playing():
            self.guild_data.add_if_empty(ctx.guild.id)
            queue: Queue = self.guild_data.get(ctx.guild.id)["queue"]

            queue.append(Song(title=url))

            # TODO: show Information about the song
            await ctx.send(**Messages.successfully(f'`{url}`', "Queued"))
            return

        await ctx.message.reply(**Messages.successfully(
            f'`{url}`',
            ":mag_right: Searching for"
        ))

        async with ctx.channel.typing():
            voice_client: VoiceClient = get(
                self.bot.voice_clients,
                guild=ctx.guild
            )

            self.guild_data.add_if_empty(ctx.guild.id)

            player = await YTDLSource.from_auto(
                ctx,
                url,
                loop=self.bot.loop,
                logger=self.logger,
                guild_data=self.guild_data,
                spotipy=self.spotipy
            )

            if isinstance(player, YTDLSource):
                await ctx.send(**Messages.playing(player.data))
                voice_client.play(
                    player,
                    after=lambda error: self.bot.loop.create_task(
                        self.player_callback(
                            error=error,
                            guild_id=ctx.guild.id,
                            channel=ctx.channel,
                            ctx=ctx
                        )
                    )
                )
                return
            await ctx.send(**Messages.error(beautify_msg(str(player))))

    @guild_only()
    @hybrid_command(
        name='playnow',
        aliases=['now', 'pnow'],
        description="Play a song now",
        usage="[url]"
    )
    @describe(
        url="The url"
    )
    async def _playnow(self, ctx: Context, *, url: str):
        await self._play(ctx, url=url)
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )

        self.guild_data.add_if_empty(ctx.guild.id)

        queue: Queue = self.guild_data.get(ctx.guild.id)["queue"]

        if not queue.is_empty():
            # Add last item to top
            queue.insert(0, queue.pop(len(queue)-1))

            # Skip
            voice_client.stop()

            if self.guild_data.get(ctx.guild.id).get("loop"):
                self.guild_data.get(ctx.guild.id)["loop"] = "skip"

    @guild_only()
    @hybrid_command(
        name='playnext',
        aliases=['pnext', "n"],
        description="Play a song next",
        usage="[url]"
    )
    @describe(
        url="The url"
    )
    async def _playnext(self, ctx: Context, *, url: str):
        await self._play(ctx, url=url)

        self.guild_data.add_if_empty(ctx.guild.id)
        queue: Queue = self.guild_data.get(ctx.guild.id)["queue"]

        if not queue.is_empty():
            # Add last item to top
            queue.insert(0, queue.pop(len(queue)-1))

    @guild_only()
    @hybrid_command(
        name='skip',
        aliases=['next', 's'],
        description="Skip the current playing song.",
        usage="[amount]"
    )
    async def _skip(self, ctx: Context, amount: int = 1):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )

        if not (voice_client and voice_client.is_playing()):
            await ctx.send(**Messages.error("There is nothing to Skip."))
            return

        queue: Queue = self.guild_data.get(ctx.guild.id)["queue"]

        for _ in range(amount - 1):  # We count from 1
            del queue[0]

        voice_client.stop()
        self.guild_data.add_if_empty(ctx.guild.id)

        if self.guild_data.get(ctx.guild.id).get("loop"):
            self.guild_data.get(ctx.guild.id)["loop"] = "skip"

        await ctx.send(**Messages.successfully("Skipped"))

    @guild_only()
    @hybrid_command(
        name='shuffle',
        aliases=['random'],
        description="Shuffles the entire queue."
    )
    async def _shuffle(self, ctx: Context):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )

        if not (voice_client and voice_client.is_playing()):
            await ctx.send(**Messages.error("The bot isn't playing anything."))
            return

        if self.guild_data.is_empty(ctx.guild.id):
            await ctx.send(**Messages.error("The queue is empty."))
            return

        queue: Queue = self.guild_data.get(ctx.guild.id)["queue"]

        if queue.is_empty():
            await ctx.send(**Messages.error("The queue is empty."))
            return

        queue.shuffle()
        await ctx.send(**Messages.successfully("Shuffled the queue :ok_hand:."))

    @guild_only()
    @hybrid_command(
        name='queue',
        aliases=['q', 'playlist', "pl"],
        description="Shows the queue.",
        usage="[page]"
    )
    @describe(
        page="one page has 10 items"
    )
    async def _queue(self, ctx: Context, page: int = 1):
        if self.guild_data.is_empty(ctx.guild.id):
            await ctx.send(**Messages.error("The queue is empty."))
            return

        page = page - 1  # We count from 1

        queue: Queue = self.guild_data.get(ctx.guild.id)["queue"]

        if queue.is_empty():
            await ctx.send(**Messages.error("The queue is empty."))
            return

        embed = Embed(
            color=Colour.blue()
        )
        embed.set_author(name="Queue")

        item_count = len(queue)
        page_count = round(item_count/10)

        if page > page_count:
            page = page_count

        items_before_current_page = page*10

        view = View(timeout=None)
        # message: discord.Message

        for key, song in enumerate(queue[
            items_before_current_page:
            items_before_current_page + 10
        ]):
            skip_to_indedx = key + items_before_current_page + 1  # We count from 1
            song: Song

            if type(song) == Song:
                title = song.title
            else:
                title = f"[{song.title}]({song.get_url()})"

            emoji = char_to_keycap[key+1]  # We count from 1

            embed.add_field(
                value=emoji + " " + title,
                name="\u200B",
                inline=False
            )

            button = Button(
                style=ButtonStyle.gray,
                emoji=emoji
            )

            button.callback = SkipButtonCallbackBuilder(
                skip_to_indedx,
                self._skip,
                view
            ).callback

            view.add_item(button)

        embed.set_footer(  # We count from 1
            text=f"Page: {page+1}/{page_count+1} • Items: {item_count}"
        )

        # TODO: ADD skip buttons
        # back_10 back reload forward forward_10

        # next_page_button = discord.ui.Button(
        #    style=discord.ButtonStyle.gray,
        #    label="->"
        # )

        # next_page_button.callback = self._queue(ctx,page=page+1)
        # view.add_item(next_page_button)

        # message =
        await ctx.send(embed=embed, view=view)

    @guild_only()
    @hybrid_command(
        name='bassboost',
        description="boot's the bass for the next music."
    )
    async def _bassboost(self, ctx: Context):
        self.guild_data.add_if_empty(ctx.guild.id)
        guild_data: dict = self.guild_data.get(ctx.guild.id)
        bassboost = guild_data.get("bassboost") or False
        guild_data["bassboost"] = not bassboost

        if bassboost:
            await ctx.send(**Messages.successfully("Disabled bassboost"))
            return

        await ctx.send(**Messages.successfully("Enabled bassboost"))

    @guild_only()
    @hybrid_command(
        name='loop',
        aliases=['repeat'],
        description="loop's current song."
    )
    async def _loop(self, ctx: Context):
        voice_client: VoiceClient = get(
            self.bot.voice_clients,
            guild=ctx.guild
        )

        if not (voice_client and voice_client.is_playing()):
            await ctx.send(**Messages.error("The bot isn't playing anything."))
            return

        self.guild_data.add_if_empty(ctx.guild.id)

        if self.guild_data.get(ctx.guild.id).get("loop"):
            del self.guild_data.get(ctx.guild.id)["loop"]
            await ctx.send(**Messages.successfully("loop Disabled"))
            return

        self.guild_data.get(ctx.guild.id)["loop"] = voice_client.source.data
        await ctx.send(**Messages.successfully("loop Enabled"))


async def setup(client):
    await client.add_cog(MusicCog(client, client.spotipy))
