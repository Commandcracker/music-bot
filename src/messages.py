#!/usr/bin/python3
# -*- coding: utf-8 -*-

# built-in modules
from datetime import timedelta

# pip modules
from discord import Embed, Colour


class Format(object):
    @staticmethod
    def date(date):
        year = date[:len(date) - 4]
        month = date[len(date) - 4:len(date) - 2]
        day = date[len(date) - 2:]
        return day + "." + month + "." + year

    @staticmethod
    def duration(duration):
        return str(timedelta(seconds=round(duration)))

    @staticmethod
    def number(number):
        return "{:,}".format(number).replace(",", ".")

    @staticmethod
    def cut_string(string, end: str = "...", length: int = 175):
        cut_string = string[0:length]
        return string if cut_string == string else cut_string + end


# TODO: Add translation for everything!
class Messages:
    @staticmethod
    def successfully(message: str, title: str = "Successfully"):
        embed = Embed(
            color=Colour.green(),
            title=title,
            description=message
        )
        return dict(embed=embed)

    @staticmethod
    def error(message: str):
        embed = Embed(
            color=Colour.red(),
            title="Error",
            description=message
        )
        return dict(embed=embed)

    @staticmethod
    def playlist(data: dict):
        embed = Embed(
            color=Colour.blue(),
            title=data.get("title"),
            url=data.get("webpage_url")
        )
        embed.set_author(name="Added Playlist to Queue")

        if data.get("uploader"):
            embed.add_field(
                name="Uploader :bust_in_silhouette:",
                value="[" + str(data.get("uploader")) + "](" +
                str(data.get("uploader_url")) + ")"
            )

        embed.add_field(
            name="Songs :notes:",
            value=str(data.get("playlist_count"))
        )

        if data.get("thumbnails"):
            thumbnails = data.get("thumbnails")
            embed.set_thumbnail(url=str(thumbnails[len(thumbnails)-1]["url"]))

        if data.get("view_count") is not None:
            embed.add_field(
                name="Views :eye:",
                value=Format.number(data.get("view_count"))
            )

        if data.get("modified_date"):
            embed.add_field(
                name="Modified Date :calendar_spiral:",
                value=Format.date(data.get("modified_date"))
            )

        if data.get("description"):
            embed.description = Format.cut_string(data.get("description"))

        return dict(embed=embed)

    @staticmethod
    def playing(data: dict):
        embed = Embed(
            color=Colour.blue(),
            title=str(data.get("title")),
            url=str(data.get("webpage_url"))
        )
        embed.set_author(name="Playing 🎶")
        embed.set_thumbnail(url=str(data.get("thumbnail")))

        if data.get("is_live"):
            embed.add_field(
                name="Duration :clock1:",
                value="LIVE"
            )
        else:
            embed.add_field(
                name="Duration :clock1:",
                value=Format.duration(data.get("duration"))
            )

        if data.get("like_count") is not None:
            embed.add_field(
                name="Likes :thumbsup: ",
                value=Format.number(data.get("like_count"))
            )

        if data.get("dislike_count") is not None:
            embed.add_field(
                name="Dislikes :thumbsdown:",
                value=Format.number(data.get("dislike_count"))
            )

        if data.get("view_count") is not None:
            embed.add_field(
                name="Views :eye:",
                value=Format.number(data.get("view_count"))
            )

        if data.get("channel"):
            embed.add_field(
                name="Channel :bust_in_silhouette:",
                value="[" + str(data.get("channel")) + "](" +
                str(data.get("channel_url")) + ")"
            )

        if data.get("upload_date"):
            embed.add_field(
                name="Upload Date :calendar_spiral:",
                value=Format.date(data.get("upload_date"))
            )
        if data.get("description"):
            embed.description = Format.cut_string(data.get("description"))

        return dict(embed=embed)

    @staticmethod
    def spotify_artist(data: dict, tracks):
        embed = Embed(
            color=Colour.blue(),
            title=data.get("name"),
            url=data.get("external_urls").get("spotify")
        )
        embed.set_thumbnail(url=str(data.get("images")[0].get("url")))
        embed.set_author(name="Added top tracks to Queue")

        embed.add_field(
            name="Followers :busts_in_silhouette:",
            value=Format.number(data.get("followers").get("total"))
        )

        embed.add_field(
            name="Songs :notes:",
            value=str(tracks)
        )

        return dict(embed=embed)

    @staticmethod
    def spotify_album(data: dict, tracks):
        embed = Embed(
            color=Colour.blue(),
            title=data.get("name"),
            url=data.get("external_urls").get("spotify")
        )
        embed.set_thumbnail(url=str(data.get("images")[0].get("url")))
        embed.set_author(name="Added album to Queue")

        # Artist
        artist_name = data["artists"][0]["name"]
        artist_url = data["artists"][0]["external_urls"]["spotify"]

        embed.add_field(
            name="Artist :bust_in_silhouette:",
            value=f"[{artist_name}]({artist_url})"
        )

        # Upload Date
        embed.add_field(
            name="Upload Date :calendar_spiral:",
            value=Format.date(data.get("release_date").replace("-", ""))
        )

        # Song count
        embed.add_field(
            name="Songs :notes:",
            value=str(tracks)
        )

        return dict(embed=embed)

    @staticmethod
    def spotify_playlist(data: dict, tracks):
        embed = Embed(
            color=Colour.blue(),
            title=data.get("name"),
            url=data.get("external_urls").get("spotify")
        )
        embed.set_thumbnail(url=str(data.get("images")[0].get("url")))
        embed.set_author(name="Added playlist to Queue")

        # Artist
        artist_name = data["owner"]["display_name"]
        artist_url = data["owner"]["external_urls"]["spotify"]

        embed.add_field(
            name="Artist :bust_in_silhouette:",
            value=f"[{artist_name}]({artist_url})"
        )

        # noinspection SpellCheckingInspection
        embed.add_field(
            name="Likes :thumbsup:",
            value=Format.number(data.get("followers").get("total"))
        )

        embed.add_field(
            name="Songs :notes:",
            value=str(tracks)
        )

        return dict(embed=embed)
