#!/usr/bin/python3
# -*- coding: utf-8 -*-
# https://en.wikipedia.org/wiki/ANSI_escape_code

# from https://gitlab.com/Commandcracker/ansi.py/-/tree/master/

ESC = '\033'

CSI = ESC + '['


def add_CSI(n) -> str:
    return CSI + str(n) + 'm'


class Background(object):
    """
    [3-bit and 4-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit)
    """
    BLACK = add_CSI(40)
    RED = add_CSI(41)
    GREEN = add_CSI(42)
    YELLOW = add_CSI(43)
    BLUE = add_CSI(44)
    MAGENTA = add_CSI(45)
    CYAN = add_CSI(46)
    WHITE = add_CSI(47)

    BRIGHT_BLACK = add_CSI(100)
    BRIGHT_RED = add_CSI(101)
    BRIGHT_GREEN = add_CSI(102)
    BRIGHT_YELLOW = add_CSI(103)
    BRIGHT_BLUE = add_CSI(104)
    BRIGHT_MAGENTA = add_CSI(105)
    BRIGHT_CYAN = add_CSI(106)
    BRIGHT_WHITE = add_CSI(107)

    DEFAULT = add_CSI(49)

    # aliases
    RESET = DEFAULT


class Foreground(object):
    """
    [3-bit and 4-bit](https://en.wikipedia.org/wiki/ANSI_escape_code#3-bit_and_4-bit)
    """
    BLACK = add_CSI(30)
    RED = add_CSI(31)
    GREEN = add_CSI(32)
    YELLOW = add_CSI(33)
    BLUE = add_CSI(34)
    MAGENTA = add_CSI(35)
    CYAN = add_CSI(36)
    WHITE = add_CSI(37)

    BRIGHT_BLACK = add_CSI(90)
    BRIGHT_RED = add_CSI(91)
    BRIGHT_GREEN = add_CSI(92)
    BRIGHT_YELLOW = add_CSI(93)
    BRIGHT_BLUE = add_CSI(94)
    BRIGHT_MAGENTA = add_CSI(95)
    BRIGHT_CYAN = add_CSI(96)
    BRIGHT_WHITE = add_CSI(97)

    DEFAULT = add_CSI(39)

    # aliases
    RESET = DEFAULT


def reset() -> str:
    """All attributes off"""
    return CSI + 'm'
