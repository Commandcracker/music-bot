#!make
include .env
export $(shell sed 's/=.*//' .env)

docker:
	docker build -t music-bot:1.0.0 -t music-bot:latest .
run:
	python src/bot.py
